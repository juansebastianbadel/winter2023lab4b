import java.util.Scanner;

public class VirtualPetApp {
	public static void main (String[] args) {
		Scanner reader = new Scanner(System.in);
		Panda[] embarrassment = new Panda[1];  //ANIMAL ARRAY

		
		for (int i = 0; i<embarrassment.length; i++) { //LOOP
		
		// Asks for user Input for all the Panda fields and putting input in a variable
			System.out.println("What is your Panda's name");
			String name = reader.nextLine();
			
			System.out.println("What is your Panda's age");
			int age = Integer.parseInt(reader.nextLine());
			
			System.out.println("What is your Panda's Martial Art");
			String martialArt = reader.nextLine();
		
			//adding the values to the constructor
			embarrassment[i] = new Panda(name, age, martialArt);

		}
		
		//Printing all field values before set call
		System.out.println(embarrassment[embarrassment.length-1].getName());
		System.out.println(embarrassment[embarrassment.length-1].getAge());
		System.out.println(embarrassment[embarrassment.length-1].getMartialArts());
		
		//set call, updating last animal name
		System.out.println("Please input an updated value for the name of the last Panda");
		embarrassment[embarrassment.length-1].setName(reader.nextLine());
		
		//Prints the 3 fields of the last inputed Panda after set call
		System.out.println(embarrassment[embarrassment.length-1].getName());
		System.out.println(embarrassment[embarrassment.length-1].getAge());
		System.out.println(embarrassment[embarrassment.length-1].getMartialArts());

	}
}