public class Panda {
	private String name;
	private int age;
	private String martialArts;
	
	public void doMartialArts() {
		if (this.martialArts != null) {
			System.out.println(this.name + " is doing " + this.martialArts + '.');
		}
		else {
			System.out.println(this.name + " can't do Martial Arts.");
		}
	}
	public void greeting() {
		System.out.println("Hi, I'm " + this.name + " and I am " + this.age + '.');
	}
	
	//LAB 4A
	//Setters
	public void setName(String name){
		this.name = name;
	}
	
	//Getters
	public String getName(){
		return this.name;
	}
	public int getAge(){
		return this.age;
	}
	public String getMartialArts(){
		return this.martialArts;
	}
	
	//constructor
	public Panda(String name, int age, String martialArts){
		this.name = name;
		this.age = age;
		this.martialArts = martialArts;
	}
}